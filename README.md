# Sample Project

# Run the tests

## Integration Tests

By default (mvn [compile|package]) only the unit tests run.   
To run the integration tests you need to run the command:

    mvn verify
    
# Don't Forget

https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-to-a-merge-request

1. On the left sidebar, select Settings > CI/CD.
1. Expand General pipelines.
2. In the Test coverage parsing field, enter JaCoCo regular expression: **Total.\*?([0-9]{1,3})%**