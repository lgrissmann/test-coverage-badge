package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

@WebMvcTest(controllers = GreetingController.class)
public class GreetingControllerTests {
    
    @Autowired
	private GreetingController controller;

    @Test
    void testGreeting() {
        assertThat(controller).isNotNull();
    }

    @Test
    void testGreetingNotNull() {
        assertThat(controller.greeting("JACU")).isNotNull();
    }

    @Test
    void testGreetingWithName() {
        Greeting hello = controller.greeting("ANA");
        assertThat(hello).isNotNull();
        assertEquals(hello.getContent(), "Hello, ANA!");
    }
}
